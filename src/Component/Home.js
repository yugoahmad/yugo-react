import React from "react";

const htmlString = 
'<!DOCTYPE html>\n'+
'<html>\n'+
'  <title>W3.CSS</title>\n'+
'  <meta name="viewport" content="width=device-width, initial-scale=1">\n'+
'  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">\n'+
'  <body>\n'+
'    <div class="w3-container w3-brown">\n'+
'      <h1>Gairah Rasa Coffee</h1>\n'+
'    </div>\n'+
'    <div class="w3-row-padding w3-margin-top">\n'+
'      <div class="w3-third">\n'+
'        <div class="w3-card">\n'+
'          <img src="https://ae01.alicdn.com/kf/H45cb046275cb4fa4865070d0351a65f8L/Jepang-Kalita-Buatan-Tangan-Kopi-Pot-Set-Berbentuk-Kipas-Drip-Filter-Kaca-Berbagi-Pot-Tiga-Lubang.jpg_q50.jpg" style="width:100%">\n'+
'          <div class="w3-container">\n'+
'            <h5>Kalita</h5>\n'+
'          </div>\n'+
'        </div>\n'+
'      </div>\n'+
'      <div class="w3-third">\n'+
'        <div class="w3-card">\n'+
'          <img src="https://cf.shopee.co.id/file/8ea17d3328290bad0969b6739d03b515" style="width:100%">\n'+
'          <div class="w3-container">\n'+
'            <h5>V60</h5>\n'+
'          </div>\n'+
'        </div>\n'+
'      </div>\n'+
'      <div class="w3-third">\n'+
'        <div class="w3-card">\n'+
'          <img src="https://cf.shopee.co.id/file/e3f134d311bc1cf6d426c5131fb9b009" style="width:100%">\n'+
'          <div class="w3-container">\n'+
'            <h5>Vietnam Drip</h5>\n'+
'          </div>\n'+
'        </div>\n'+
'      </div>\n'+
'    </div>\n'+
'    <div class="w3-row-padding w3-margin-top">\n'+
'      <div class="w3-third">\n'+
'        <div class="w3-card">\n'+
'          <img src="https://majalah.ottencoffee.co.id/wp-content/uploads/2017/08/espresso-crema.jpg" style="width:100%">\n'+
'          <div class="w3-container">\n'+
'            <h5>Espresso</h5>\n'+
'          </div>\n'+
'        </div>\n'+
'      </div>\n'+
'      <div class="w3-third">\n'+
'        <div class="w3-card">\n'+
'          <img src="https://cf.shopee.co.id/file/fb778b718156b849dc946dd6ec2e5cbc" style="width:100%">\n'+
'          <div class="w3-container">\n'+
'            <h5>Japanese</h5>\n'+
'          </div>\n'+
'        </div>\n'+
'      </div>\n'+
'      <div class="w3-third">\n'+
'        <div class="w3-card">\n'+
'          <img src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full/otten-coffee_kalita-wave-style-dripper-set_full01.jpg" style="width:100%">\n'+
'          <div class="w3-container">\n'+
'            <h5>Wave</h5>\n'+
'          </div>\n'+
'        </div>\n'+
'      </div>\n'+
'    </div>\n'+
'  </body>\n'+
'</html>';

const Home = () => {

    return (
        <div dangerouslySetInnerHTML={{ __html: htmlString }} />
    )
}

export default Home;