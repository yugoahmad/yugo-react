import React, { useState } from 'react';
import axios from 'axios';
import { useHistory } from 'react-router-dom'

const Login = () => {
    
 const[username,setUsername] = useState('');
 const[password,setPassword] = useState('');
 const [error, setError] = useState('');
 const history = useHistory();

 const onChangeuser = (e) => {
     const value= e.target.value
     setUsername(value)
 }

 const onChangepass = (e) => {
    const value = e.target.value
    setPassword(value)
 }

 const submitLogin = () => {
    const data = {
        username: username,
        password: password
    }

    // console.log(data)
    axios.post('https://tasklogin.herokuapp.com/api/login', data)
        .then(result => {
                history.push('/Home');
        })
        .catch(e => {
            alert("Username atau Password salah. Tolong cek kembali data anda");
            setError(e.response.status)
        })
    }

    return (
        <div style={{alignItems: 'center', marginTop: 70}}>
            <div className="container">
                <div className="row justify-content-center">
                    
                    <div className="col-md-5">
                    <div className="card-body">
                    
                        <div className="card p-4">
                        <h2 style={{textAlign:'center'}} >Login</h2>
                        <div style={{textAlign: 'center'}}>
                            <img src="https://www.zpropmortgage.com/wp-content/uploads/2019/12/icon3-150x150.png"/>
                        </div>
                        
                                <div className="form-group">
                                    <label>Username</label>
                                    <input type="text" placeholder="Username" className="form-control" value={username} onChange={onChangeuser}/>
                                </div>

                                <div className="form-group">
                                    <label>Password</label>
                                    <input type="password" placeholder="Password" className="form-control" value={password} onChange={onChangepass}/>
                                </div>

                                <button style={{marginTop: 10}} className="btn btn-primary" onClick={submitLogin} >Login</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default Login;