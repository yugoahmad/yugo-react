import { BrowserRouter, Route, Switch } from "react-router-dom";
import React from 'react';
import Login  from './Component/Login';
import Home from "./Component/Home";

function App() {
  return (
    <BrowserRouter>
      <div className="bg-blue-100 w-full">
        <div className="container w-full mx-auto px-4 pt-2 lg:pt-5">
          <Switch>
            <Route path="/" component={Login} exact />
            <Route path="/Home" component={Home} exact />
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
